var express = require('express');
var app = express();

// direcionar todo o trafico de / para a pasta public
app.use('/', express.static('public'));

app.listen(process.env.PORT || 3000, function () {
  console.log('Exemplo de app porta 3000');
});
